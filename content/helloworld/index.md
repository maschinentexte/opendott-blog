+++
title = "Hello!"
description = "This blog is just starting; so welcome to a collection of posts."
date = 2019-12-09
draft = false
+++

This blog is going live. 😬 The feeling of actually putting this online and sharing with the world is exciting. It is also my first ever blog. I have not had one ever since it was a thing. Even not when the form of blogging came about and I was studying digital media communication design.

Well, here are: I am studying again and for the PhD with the [OpenDoTT][] project a blog seems a good way to document the journey along the way, keep up good writing habits, share useful discoveries with context and last but not least practice to work in the open.

<!-- more -->

I have chosen to use [Zola][] as a blogging tool. It is a static site generator written in [Rust][] and suits my needs with Markdown written in plain text files, archived in a Git repository.

I am just getting started with Zola and hope to master it along the way. For now, the theme is a mixture of two themes made by the Zola team: _Hyde_ and _After Dark_. Writing more SASS and HTML templates should be a good time to time destraction from research.

If you want to see the code to this blog, it is open source and can be found in this repository: [gitlab.com/maschinentexte/opendott-blog][blog repo]

Until the next post!

[OpenDott]: https://opendott.org
[blog repo]: https://gitlab.com/maschinentexte/opendott-blog
[Zola]: https://getzola.org
[Rust]: https://rust-lang.org