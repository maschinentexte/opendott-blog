+++
title = "How to blog with Zola"
description = "A short introduction on hwo to use Zola as a static generated blogging platform"
date = 2019-12-09
draft = true

[taxonomies]
categories = ["Tutorials"]
tags = ["howto"]
+++

![An image of the internet of things](https://fscl01.fonpit.de/userfiles/1799474/file/IoT/Internet-of-things-hero.jpg)

This is a summary of this post. It will be taken until the following HTML comment line you can see in here in Markdown:

<!-- more -->

And this content is then visible on the page view. There can also be images. This we will try next.

![An image of the internet of things](https://fscl01.fonpit.de/userfiles/1799474/file/IoT/Internet-of-things-hero.jpg)